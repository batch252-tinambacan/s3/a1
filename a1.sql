
-- Create users table
CREATE TABLE users (
	id INT NOT NULL AUTO_INCREMENT,
	email VARCHAR(50),
	password VARCHAR(50) NOT NULL,
	datetime_created DATETIME NOT NULL,
	PRIMARY KEY (id)
);

-- Create posts table
CREATE TABLE posts (
	id INT NOT NULL AUTO_INCREMENT,
	title VARCHAR(50),
	content VARCHAR(50) NOT NULL,
	datetime_created DATETIME NOT NULL,
	user_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_posts_user_id
		FOREIGN KEY (user_id) REFERENCES users(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

-- Create new records in users table
INSERT INTO users (email, password, datetime_created) VALUES ("johnsmith@gmail.com", "passwordA", "2021-01-01 01:00:00");
INSERT INTO users (email, password, datetime_created) VALUES ("juandelacruz@gmail.com", "passwordB", "2021-01-01 02:00:00");
INSERT INTO users (email, password, datetime_created) VALUES ("janesmith@gmail.com", "passwordC", "2021-01-01 03:00:00");
INSERT INTO users (email, password, datetime_created) VALUES ("mariadelacruz@gmail.com", "passwordD", "2021-01-01 04:00:00");
INSERT INTO users (email, password, datetime_created) VALUES ("johndoe@gmail.com", "passwordE", "2021-01-01 05:00:00");

-- Create new records in posts table
INSERT INTO posts (title, content, datetime_created, user_id) VALUES ("First Code", "Hello World", "2021-01-02 01:00:00", 1);
INSERT INTO posts (title, content, datetime_created, user_id) VALUES ("Second Code", "Hello Earth!", "2021-01-02 02:00:00", 1);
INSERT INTO posts (title, content, datetime_created, user_id) VALUES ("Third Code", "Welcome to Mars", "2021-01-02 03:00:00", 2);
INSERT INTO posts (title, content, datetime_created, user_id) VALUES ("Fourth Code", "Bye Bye Solar System", "2021-01-02 04:00:00", 4);

-- Retrieve all posts from author ID 1
SELECT * FROM posts WHERE user_id = 1;

-- Retrieve all user's email and datetime of creation
SELECT email, datetime_created FROM users;

-- Update post from "Hello Earth" to "Hello to the people of the Earth!" using records id
UPDATE posts SET content = "Hello to the people of the Earth!" WHERE id = 2;

-- Delete user "johndoe@gmail.com"
DELETE FROM users WHERE email = "johndoe@gmail.com";
SELECT * FROM users;

-- SELECT * FROM posts WHERE content like "Hello%";